#!/usr/bin/python3

#   Este programa baja videos de youtube
#   Copyright (C) 2022 Cristian Tocci
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.

#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.

#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <https://www.gnu.org/licenses/>.

#   Contacto : toccicristian@hotmail.com / toccicristian@protonmail.ch


licencias=dict()
licencias['gplv3']="""    pitsydownloader.py  Copyright (C) 2022  Cristian Tocci
    This program comes with ABSOLUTELY NO WARRANTY; for details press 'w'.
    This is free software, and you are welcome to redistribute it
    under certain conditions; See COPYING.odt file for further details.
"""
licencias['gplv3logo']="""
+----------------------------------------------------------------------------------------------------+
|oooooooo+~~~+~~~~~~+~~~~~+~~~~+~~~~~~+~~~~+~~~~~~+~~+~~~~+~~~~~+~~~~+~~~~~~++~~+~~+~~~~~~:  ::::::~+|
|oooooooo :::::::::::::::::::::::::::::::::::::::::::~::::::::::::::::::::::::::::::::. ~~~++ooooo+:.|
|ooooooo~::::::~:::::::::::::::::::::::::::::::::::::+::::::::::::::::::::::::~~.~:~:~+oooooooooooo:.|
|ooooooo :~:~~~~~~~~~~+~::: +~~~~~~~~~~~~~::++ :::::~+~:::::::::::::::::::~...~:::~ooooooooooooooo~.+|
|oooooo~~:~oo~~~~~~~~~oo~:~+oo ~~~~~~.ooo.~oo+~::::.+o ::::::::::::::::~  .~::::+oo+~:   +ooooooo::+o|
|oooooo::.+o+~::::::~+oo : oo~::::::::oo~:~oo~::::: oo~:::::::::::::: ~ ~::::.++~ ~:::::.+oooo+~ ~ooo|
|ooooo+~:~oo~:::::::::::::~oo::::::::+oo :+oo~:::::~oo+.::::::::::.:~ ~:::::: .:::::::~~oooo+:~ +oooo|
|ooooo::~+o+.:::::::::::: oo+~:::::: oo~~:oo~::::::~ooo~::::::::.~~.::::::::::::::::~~+oooooo+~::oooo|
|oooo+~::oo~:::~:~:~~::::~oo~       ~oo::+oo.::::::~ooo+~::::: ~~.:::::::::::::::: ~+oooooooooo~~oooo|
|oooo~::+oo :::~   +oo::.ooo~~~~~~~~~:.: oo+:::::::~oooo~:::~~+:::::::::::::::: ~+++~~~~oooooo+.~oooo|
|ooo+.: oo~:::::::.oo+.:~oo~::::::::::::~oo:::::::::oooo+~::++~::::::::::::::~   .::::::ooooo~.~ooooo|
|ooo~::~oo::::::::~oo~:~+o+~::::::::::: oo+~:::::::.+ooo~.~o+:::::::::::::::::::::::: +oooo+: +oooooo|
|ooo.: oo+.~~~~~~ +oo.::oo~::::::::::::~oo~~~~~~~:::+oo~ +oo ::::::::::::::::::::.:~ooooo+: ~oooooooo|
|oo~::.~~~~~~~~~~~~~ ::~+~.::::::::::::~+~~~~~~~~~:::o~ +ooo:::::::::::::::::: ~+oooooo~::~oooooooooo|
|o+ :~   ~::::::::::::.  ~::::: ..:::::::::::::::::::~ ~oooo~~::::::::::~. ~~+oooooo+~::+oooooooooooo|
|o~~:~~: ~ :~~. ~~.::~~~~. ::.~~~~::~:: :~~.~::~~ ::::.oooooo+~~::::~~~~ooooooooo+~::~+oooooooooooooo|
|o::~~~~:::~~~ ~~~.:: ::~.~:~.~~~: ~~~ :~~~: ~~~~~:::: oooooooooooooooooooooo++~::~+ooooooooooooooooo|
|+:::~::::::~~::::::::~~:::~::~:::::::::::~::::~:::::::~ooooooooooooooooo++~::~~+oooooooooooooooooooo|
|::::::::::::::::::::::::::::::::::::::::::::::::::::::: ~oooooooooo+~~~::~~+oooooooooooooooooooooooo|
|~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~:~~~~~:    ::::::::~~~ooooooooooooooooooooooooooooo|
+----------------------------------------------------------------------------------------------------+
"""
licencias['textow']=""" 
    15. Disclaimer of Warranty.
    THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY 
    APPLICABLE LAW. EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT 
    HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM “AS IS” WITHOUT 
    WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT 
    LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A 
    PARTICULAR PURPOSE. THE ENTIRE RISK AS TO THE QUALITY AND PERFORMANCE 
    OF THE PROGRAM IS WITH YOU. SHOULD THE PROGRAM PROVE DEFECTIVE, YOU 
    ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.
    
    16. Limitation of Liability.
    IN NO EVENT UNLESS REQUIRED BY APPLICABLE LAW OR AGREED TO IN WRITING 
    WILL ANY COPYRIGHT HOLDER, OR ANY OTHER PARTY WHO MODIFIES AND/OR 
    CONVEYS THE PROGRAM AS PERMITTED ABOVE, BE LIABLE TO YOU FOR 
    DAMAGES, INCLUDING ANY GENERAL, SPECIAL, INCIDENTAL OR CONSEQUENTIAL 
    DAMAGES ARISING OUT OF THE USE OR INABILITY TO USE THE PROGRAM 
    (INCLUDING BUT NOT LIMITED TO LOSS OF DATA OR DATA BEING RENDERED 
    INACCURATE OR LOSSES SUSTAINED BY YOU OR THIRD PARTIES OR A FAILURE OF 
    THE PROGRAM TO OPERATE WITH ANY OTHER PROGRAMS), EVEN IF SUCH HOLDER 
    OR OTHER PARTY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

    17. Interpretation of Sections 15 and 16.
    If the disclaimer of warranty and limitation of liability provided above 
    cannot be given local legal effect according to their terms, 
    reviewing courts shall apply local law that most closely approximates 
    an absolute waiver of all civil liability in connection with the Program, 
    unless a warranty or assumption of liability accompanies a copy of 
    the Program in return for a fee.
    """

import yt_dlp
import requests
import os
import sys
import time
from tkinter import ttk
from tkinter import filedialog
import tkinter as tk
import threading


ancho_barra_progresion=550
default_dir='~'


class FormatoVideo :
	def __init__(self,tam=int(),ext=str(),formato=str(),acodec=str(),url=str()):
		if tam is None:
			tam=0
		if ext is None:
			ext=''
		if formato is None:
			formato=''
		if url is None:
			url=''
		self._tam=tam
		self._ext=ext
		self._formato=formato
		self._acodec=acodec
		self._url=url
	
	def get_tam (self,mb=False):
		if mb:
			return round(self._tam/1024/1024,2)
		return self._tam
	
	def get_ext (self):
		return self._ext
	
	def get_formato (self):
		return self._formato
	
	def get_acodec (self):
		return self._acodec
	
	def get_url(self):
		return self._url
	
	def to_str(self,url_longitud=None):
		u=self._url
		lj=0
		if url_longitud is not None and str(url_longitud).isdigit() and int(url_longitud)>0:
			u=self.get_url()[:url_longitud]+'[...]'
			lj=int(url_longitud)+3
		return (
			(str(self.get_tam(mb=True))+' MB ').rjust(15)+
			str(self.get_ext().ljust(8))+
			str(self.get_formato().ljust(20))+
			str(u.ljust(lj))
			)


def get_formatos(video):
	formatos=list()
	for f in video['formats']:
		sz=0
		if 'filesize' in f.keys() and f['filesize'] is not None:
			sz=int(f['filesize'])
		formatos.append(FormatoVideo(tam=sz,ext=str(f['ext']),formato=str(f['format']).split(' - ')[1],acodec=str(f['acodec']),url=str(f['url'])))
	return formatos
		

def get_videos(url):
	ytdl=yt_dlp.YoutubeDL({'outtmpl': '%(id)s.%(ext)s'})
	with ytdl:
		resultado = ytdl.extract_info(url,download=False)
	v=list()
	if 'entries' not in resultado:
		v.append(resultado)
		return v
	for entrada in resultado['entries']:
		v.append(entrada)
	return v


def acorta_texto(texto=str(),longitud=int()):
	if longitud is None:
		longitud=30
	if texto is None:
		return '...'
	if len(texto)<longitud:
		return texto
	return texto[:longitud]+'...'


def directory_browser(titulo=str(),defaultdir=str()):
        if not titulo:
                titulo='Seleccione directorio destino...'
        directorio=filedialog.askdirectory(title=titulo)
        if not directorio:
               directorio=defaultdir 
        return os.path.expanduser(os.path.normpath(directorio))


def examinar(entry_url):
	head_url=os.path.expanduser(os.path.normpath(default_dir))
	directorio_seleccionado=directory_browser('Seleccione directorio destino...',entry_url.get())
	entry_url.delete(0,tk.END)
	if not directorio_seleccionado:
		directorio_seleccionado=head_url
	entry_url.insert(tk.END, directorio_seleccionado)
	return None


# ######################################################################### INTERACCIONES DEL MOUSE
def copiapega_menu (event,menu):
	try:
		menu.tk_popup(event.x_root,event.y_root)
	finally:
		menu.grab_release()


def copiar_al_portapapeles(ventana_principal,entrada):
	if entrada.select_present():
		texto=entrada.selection_get()
		ventana_principal.clipboard_clear()
		ventana_principal.clipboard_append(texto)
	
	
def cortar_al_portapapeles(ventana_principal,entrada):
	if entrada.select_present():
		texto_copiado=entrada.selection_get()
		ventana_principal.clipboard_clear()
		ventana_principal.clipboard_append(texto_copiado)
		textofinal=entrada.get().replace(entrada.selection_get(),'')
		selstartpos=entrada.index(tk.INSERT)-len(entrada.selection_get())
		entrada.delete(0,tk.END)
		entrada.insert(0,textofinal)
		entrada.icursor(selstartpos)
		return None
		

def pegar_del_portapapeles(ventana_principal,entrada):
	portapapeles = ventana_principal.clipboard_get()
	if entrada.select_present():
		textofinal=entrada.get().replace(entrada.selection_get(),portapapeles)
		selstartpos=entrada.index(tk.INSERT)-len(entrada.selection_get())
		entrada.delete(0,tk.END)
		entrada.insert(0,textofinal)
		entrada.icursor(selstartpos+len(portapapeles))
		return None
	entrada.insert(entrada.index(tk.INSERT),portapapeles)
	return None
	
	
def copiapega_menu_destruye(menu,event=None):
	menu.unpost()
# ################################################################# FIN DE INTERACCIONES DEL MOUSE


def cancelar_descarga():
	global continuar_descarga
	continuar_descarga=False
	return None


def reset_ui_info(barra_progresion,progresion_info,label_estado_descarga,statmsg=str()):
	if statmsg == None:
		statmsg='-'
	progresion_info.config(text='inactiva')
	barra_progresion.step(0)
	label_estado_descarga.config(text=statmsg)
	return None


continuar_descarga=False
def bajar_video(video,formato,entry_destino,barra_progresion,progresion_info,label_estado_descarga):
	global continuar_descarga
	continuar_descarga=True
	chunksz=4096
	destino_url=os.path.join(entry_destino.get(),video['title']+'-'+
	    formato.get_formato().replace(' ','-').replace('(','-').rstrip(')')+'.'+formato.get_ext())
	with open (destino_url,'wb') as ar_destino:
		agent={'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.159 Safari/537.36', 'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8', 'Accept-Language': 'en-us,en;q=0.5', 'Sec-Fetch-Mode': 'navigate'}
		respuesta=requests.get(formato.get_url(),stream=True,headers=agent)
		tam_total=respuesta.headers.get('content-length')
		if tam_total is None:
			ar_destino.write(respuesta.content)
		else:
			barra_progresion['value']=0
			label_estado_descarga.config(text='Descargando :'+acorta_texto(str(video['title']),40))
			dl=0
			tam_total = int (tam_total)
			tam_totalmb=round(tam_total/1024/1024,2)
			for datos in respuesta.iter_content(chunk_size=chunksz):
				if not continuar_descarga:
					reset_ui_info(barra_progresion,progresion_info,label_estado_descarga,
					statmsg = 'CANCELADA :' + acorta_texto(str(video['title']),39))
					return None
				dl += len(datos)
				ar_destino.write(datos)
				terminado = int(ancho_barra_progresion*dl/tam_total)
				dlmb=round(dl/1024/1024,2)
				barra_progresion.step(chunksz*99/tam_total)
				progresion_info.config(text='['+str(dlmb)+'/'+str(tam_totalmb)+']')
			barra_progresion['value']=100
			label_estado_descarga.config(text='TERMINADA :'+acorta_texto(str(video['title']),40))
			progresion_info.config(text='inactiva')
	return None


def selecciona_formato_preferido(video,soloaudio=False,calidad=('(480p)','(720p)'),ext=['webm','mp4']):
	formatos=get_formatos(video)
	f_temp=False
	if soloaudio:
		for formato in formatos[::-1]:
			if formato.get_formato().startswith('audio only'):
				if not f_temp:
					f_temp=formato
				if formato.get_tam() > f_temp.get_tam():
					f_temp=formato
		return f_temp
	for formato in formatos:
		if formato.get_formato().endswith(calidad) and (formato.get_ext() in ext) and formato.get_acodec() != 'none':
			return formato
	for formato in formatos[::-1]:
		if not formato.get_formato().startswith('audio only') and formato.get_acodec() != 'none':
			return formato
	for formato in formatos[::-1]:
		if not formato.get_formato().startswith('audio only'):
			return formato
	return False


def obtiene_lista_videos(entry_url,label_estado_descarga):
	try:
		s=''
		label_estado_descarga.config(text='Buscando video...')
		videos=get_videos(entry_url.get().split('&')[0])
		if len(videos)>1:
			s='s'
		label_estado_descarga.config(text=str(len(videos))+' Video'+s+' Encontrado'+s)
	except yt_dlp.utils.DownloadError:
		label_estado_descarga.config(text='Video no encontrado')
		return None
	return videos


def gestion_de_bajada(entry_url,entry_destino,barra_progresion,progresion_info,label_estado_descarga,soloaudio=False):
	if not entry_url.get().startswith('https://www.youtube.com/watch?v='):
		return None
	lista_videos=obtiene_lista_videos(entry_url,label_estado_descarga)
	if lista_videos is None:
		return None
	video=lista_videos[0]
	formato_preferido=selecciona_formato_preferido(video=video,soloaudio=soloaudio)
	try:
		bajar_video(video,formato_preferido,entry_destino,barra_progresion,progresion_info,label_estado_descarga)
	except (ConnectionError) as e:
		reset_ui_info(barra_progresion,progresion_info,label_estado_descarga,statmsg = 'ERROR EN DESCARGA : REINTENTAR?')
	return None


def comando_boton_bajar (soloaudio_var,entry_url,entry_destino,barra_progresion,progresion_info,label_estado_descarga):
	soloaudio=False
	if soloaudio_var ==1:
		soloaudio=True
	threading.Thread(target=gestion_de_bajada,args=(entry_url,entry_destino,barra_progresion,progresion_info,
					   label_estado_descarga,soloaudio,)).start()
	return None


def show_w(ventana_principal,textow):
        ventana_w = tk.Toplevel(ventana_principal)
        ventana_w.title('This program comes with ABSOLUTELY NO WARRANTY')
        ventana_w.geometry('800x600')
        tk.Label(ventana_w,text=textow).pack()
        ventana_w.focus_set()
        ventana_w.bind('<Escape>', lambda event : ventana_w.destroy())
    

def ayuda(ventana_principal):
        texto_ayuda="""
        Pitsy Downloader
        -----------------------------------------------------
        F1 : Esta ayuda.
        Enter : Descarga el video/audio de la url ingresada.
        Esc : Cierra la aplicación / Cierra esta ventana
        """
        ventana_ayuda = tk.Toplevel(ventana_principal)
        ventana_ayuda.title(' Atajos y ayuda')
        tk.Label(ventana_ayuda,text=texto_ayuda,justify='left').pack(side=tk.LEFT,padx=(0,30), pady=(10,10))
        ventana_ayuda.focus_set()
        ventana_ayuda.bind('<Escape>', lambda event : ventana_ayuda.destroy())


def muestra_ventana():
	ventana_principal_min_ancho=700
	ventana_principal_min_alto=300
	ventana_principal = tk.Tk()
	ventana_principal.title('Pitsy Downloader')
	ventana_principal.geometry(str(ventana_principal_min_ancho)+'x'+str(ventana_principal_min_alto))
	ventana_principal.resizable(width=False, height=False)
	
	marco_info = tk.Frame(ventana_principal)
	label_info = tk.Label(marco_info,text='F1:Ayuda')
	boton_w = tk.Button(marco_info,text='Acerca de...')
		
	marco_bajar = tk.Frame(ventana_principal,width=str(ventana_principal_min_ancho-20),height='100')
	marco_url_bajar = tk.Frame(marco_bajar,width=str(ventana_principal_min_ancho-100),height='100')
	label_url_bajar = tk.Label(marco_url_bajar,text='URL:',width=5)
	entry_url_bajar = tk.Entry(marco_url_bajar,width=str(int(ventana_principal_min_ancho/9)))
	menu_rmb_entry_url_bajar=tk.Menu(entry_url_bajar,tearoff=0)

	marco_boton_bajar = tk.Frame(marco_bajar,width='100',height='100')
	boton_bajar = tk.Button(marco_boton_bajar,text='BAJAR')
	
	marco_opciones = tk.Frame(ventana_principal,width=str(ventana_principal_min_ancho-20),height='100')
	marco_casillas = tk.Frame(marco_opciones,width=str(ventana_principal_min_ancho-100),height='100')
	soloaudio_var = tk.IntVar()
	soloaudio_checkbox = tk.Checkbutton(marco_casillas,text='Solo Audio',variable=soloaudio_var)
	marco_boton_cancelar = tk.Frame(marco_opciones,width='100',height='100')
	boton_cancelar = tk.Button(marco_boton_cancelar,text='CANCELAR')
	
	
	marco_destino = tk.Frame(ventana_principal,width=str(ventana_principal_min_ancho-20),height='100')
	marco_destino_url = tk.Frame(marco_destino,width=str(ventana_principal_min_ancho-100),height='100')
	label_destino_url = tk.Label(marco_destino_url,text='Destino:',width=9)
	entry_destino_url = tk.Entry(marco_destino_url,width=str(int(ventana_principal_min_ancho/9)))
	marco_destino_boton_examinar = tk.Frame(marco_destino,width='100',height='100')
	boton_examinar = tk.Button(marco_destino_boton_examinar,text='Examinar...')
	
	marco_barra_progresion = tk.Frame(ventana_principal,width=str(ventana_principal_min_ancho-20),height='100')
	marco_barra_progresion_barra = tk.Frame(marco_barra_progresion,width=str(ventana_principal_min_ancho-100))
	label_estado_descarga=tk.Label(marco_barra_progresion_barra,width='50',text='-')
	barra_progresion = ttk.Progressbar(marco_barra_progresion_barra,length=ancho_barra_progresion,mode='determinate')
	marco_barra_progresion_info = tk.Frame(marco_barra_progresion, width='100',height='100')
	label_barra_progresion_info = tk.Label(marco_barra_progresion_info,text='[inactiva]')
	
	boton_w.config(command= lambda :show_w(ventana_principal,licencias['textow']))
	ventana_principal.bind('<F1>', lambda event : ayuda(ventana_principal))
	ventana_principal.bind('<Escape>', lambda event : ventana_principal.destroy())
	boton_bajar.config(command= lambda:comando_boton_bajar(soloaudio_var.get(),
		entry_url_bajar,entry_destino_url,barra_progresion,label_barra_progresion_info,label_estado_descarga))
	entry_url_bajar.bind('<Return>',lambda x:comando_boton_bajar(soloaudio_var.get(),
		entry_url_bajar,entry_destino_url,barra_progresion,label_barra_progresion_info,label_estado_descarga))
	boton_cancelar.config(command = lambda :cancelar_descarga())
	boton_examinar.config(command = lambda :examinar(entry_destino_url))
	menu_rmb_entry_url_bajar.add_command(label='Copiar - Ctrl+c', command = lambda :copiar_al_portapapeles(ventana_principal,entry_url_bajar))
	menu_rmb_entry_url_bajar.add_command(label='Cortar - Ctrl+x', command = lambda :cortar_al_portapapeles(ventana_principal,entry_url_bajar))
	menu_rmb_entry_url_bajar.add_command(label='Pegar - Ctrl+v', command = lambda :pegar_del_portapapeles(ventana_principal,entry_url_bajar))
	menu_rmb_entry_url_bajar.bind('<FocusOut>', lambda event: copiapega_menu_destruye(menu_rmb_entry_url_bajar,event=None))
	entry_url_bajar.bind('<Button-3>',lambda event:copiapega_menu(event, menu_rmb_entry_url_bajar))
	
	marco_info.pack(side='top', fill='x')
	label_info.pack(side=tk.LEFT,padx=(10,10))
	boton_w.pack(side=tk.LEFT,padx=(10,10))
	
	marco_bajar.pack(side='top', fill='x')
	marco_url_bajar.pack(side='left',anchor=tk.W)
	label_url_bajar.pack(side='top',padx=(5,0),anchor=tk.W)
	entry_url_bajar.pack(side='top', padx=(10,10))
	entry_url_bajar.focus()
	marco_boton_bajar.pack(side='right')
	boton_bajar.pack(side='top',padx=(10,10),pady=(25,10))
	
	marco_opciones.pack(side='top', fill='x')
	marco_casillas.pack(side='left',anchor=tk.W)
	soloaudio_checkbox.pack(side='left',padx=(5,5),anchor=tk.W)
	marco_boton_cancelar.pack(side='right')
	boton_cancelar.pack(side='top',padx=(10,10),pady=(0,10))
	
	marco_destino.pack(side='top', fill='x')
	marco_destino_url.pack(side='left',anchor=tk.W)
	label_destino_url.pack(side='top',padx=(5,0),anchor=tk.W)
	entry_destino_url.pack(side='top',padx=(10,10))
	entry_destino_url.insert(0,os.path.expanduser(os.path.normpath(default_dir)))
	marco_destino_boton_examinar.pack(side='right')
	boton_examinar.pack(side='top', padx=(10,10), pady=(25,10))
	
	marco_barra_progresion.pack(side='top',fill='x')
	marco_barra_progresion_barra.pack(side='left',padx=(10,10),pady=(15,15))
	label_estado_descarga.pack(side='top')
	barra_progresion.pack(side='left',padx=(0,0),pady=(10,10))
	marco_barra_progresion_info.pack(side='left',fill='x')
	label_barra_progresion_info.pack(side='left', padx=(0,0), pady=(25,10))
	
	ventana_principal.mainloop()


muestra_ventana()
