# pitsydownloader

Es una herramienta muy básica y sin opciones para descargar videos de youtube.


# USO:
 1. Pegar la direccion del video en 'URL'.
 2. Usar 'Examinar' para seleccionar la carpeta en la que queremos bajarlo.
 3. Hacer click en 'BAJAR'.


## Cambios planeados
 Tengo la intención de agregarle la posibilidad de convertir los videos descargados a otros formatos.


## Licencia
Este programa se encuentra licenciado bajo la GPLv3.0


## Estado del Proyecto
Este programa es algo que vine haciendo en un poco de tiempo libre,
por lo cual puede que pase un tiempo considerable antes de que haya novedades.
